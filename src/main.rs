#![allow(unused)]

fn main() {
    let u_x = 56_u8;
    let b_u_x = 56_i16;
    let f_x = 56.0_f32;
    let b_f_x = 56.0_f64;

    let u_x: u8 = 56_u8;
    let b_u_x: i16 = 56_i16;
    let f_x: f32 = 56.0_f32;
    let b_f_x: f64 = 56.0_f64;

    let u_x: u8 = 56;
    let b_u_x: i16 = 56;
    let f_x: f32 = 56.0;
    let b_f_x: f64 = 56.0;

    // Does theses are the same ????

    let sum = 5 + 10;
    let diff = 95.5 - 6.0;
    let prod = 4 * 50;
    let quotient = 56.7 / 32.2;
    let remainder = 43 % 5;

    println!("5 + 10 = {}", sum);
    println!("95.5 - 6 = {}", diff);
    println!("4 * 50 = {}", prod);
    println!("56.7 / 32.2 = {}", quotient);
    println!("43 % 5 = {}", remainder);

    let boolean_var = true;
    let var: bool = false;

    println!("This is {}, and this is {}.", boolean_var, var);

    let c_char: char = 'z';
    let z_char: char = 'ℤ';
    let heart_eyed_cat: char = '😻';

    println!(
        "What the chars : {}, {}, {}",
        c_char, z_char, heart_eyed_cat
    );

    let tup = (5000, 64, 1);
    let (x, y, z) = tup;

    println!("The values of x, y, z, are : {}, {}, {}.", x, y, z);
    println!("The values of tup are : {}, {}, {}.", tup.0, tup.1, tup.2);

    let g = [3; 5];

    println!("G[1] = {}", g[1]);
}
